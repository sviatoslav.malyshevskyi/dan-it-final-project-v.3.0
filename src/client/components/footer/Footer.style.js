import styled from 'styled-components';

export const FooterMainWrapper = styled.footer`
  position: absolute;
  bottom: 0;
  left: 0;
  width: 100%;
  background-color: #002464;
  user-select: none;
`;

export const FooterTopContainer = styled.div`
  display: flex;
  height: 250px;
  margin-top: 2%;
  margin-bottom: 2%;
  align-items: center;
  justify-content: center;
`;

export const FooterMenuContainer = styled.div`
  display: flex;
  width: 50%;
  margin-left: 5%;
`;

export const FooterMenuBox = styled.ul`
  display: flex:
  flex-direction: column;
  margin: 0;
  padding: 0;
  list-style: none;
  align-items: center;

    @media screen and (max-width: 1000px) {
         width: 200px;
    }
`;

export const FooterMenuItem = styled.li`
  margin-left: 30px;
  font: 500 15px Merriweather, Lato, sans-serif;
  line-height: 32px;
  text-transform: uppercase;
  color: #ffffff;

  &:hover {
    cursor: pointer;
    color: #C58F00;
    transform-origin: left;
    filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25));
  }

   @media screen and (max-width: 1000px) {
        font-size: 13px
    }

   @media screen and (max-width: 900px) {
        font-size: 12px;
        line-height: 25px;
   }
`;

export const MailAndFollowContainer = styled.div`
  margin-right: 5%;
`;

export const SendMailBox = styled.div`
  width: 500px;

    @media screen and (max-width: 1000px) {
      margin-left: 5%;
    }

  & h3 {
    margin-bottom: 10px;
    font: 500 16px Merriweather, Lato, sans-serif;
    color: #ffffff;
      @media screen and (max-width: 1000px) {
        margin-left: 10%;
      }
  }

  & input {
    width: 340px;
    height: 36px;
    padding-left: 20px;
    color: #ffffff;
    background: rgba(255, 255, 255, 0.15);
    border: none;

    &:hover {
      background: rgba(255, 255, 255, 1);
    }

    &:focus {
      font: 500 13px Merriweather, Lato, sans-serif;
      border: 2px outset #C58F00;
      border-radius: 4px;
      background: rgba(255, 255, 255, 1);
      color: #000000;
    }

    @media screen and (max-width: 1000px) {
        width: 250px;
        margin-left: 10%;
    }
  }

  & button {
    width: 110px;
    height: 36px;
    color: #002464;
    background: #ffffff;
    border: 2px solid #ffffff;
    box-sizing: border-box;
    border-radius: 0;
  }
`;

export const FollowBox = styled.div`
  margin-top: 50px;

     @media screen and (max-width: 1000px) {
        margin-left: 8%;
      }
`;

export const FollowIconsBar = styled.div`
  display: flex;
  width: 400px;
  align-items: center;
  justify-content: space-evenly;
`;
