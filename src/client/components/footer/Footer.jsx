import React from 'react';
import {
  FollowBox, FollowIconsBar, FooterMenuBox,
  FooterMenuContainer, FooterMenuItem, FooterTopContainer,
  MailAndFollowContainer, SendMailBox, FooterMainWrapper,
} from './Footer.style';
import UseAnimations from 'react-useanimations';
import facebook from 'react-useanimations/lib/facebook';
import instagram from 'react-useanimations/lib/instagram';
import twitter from 'react-useanimations/lib/twitter';
import linkedin from 'react-useanimations/lib/linkedin';
import youtube from 'react-useanimations/lib/youtube';
import Love from '../misc/designed-and-developed';
import CopyrightContainer from '../misc/copyright/Copyright';

const Footer = () => {
  return (
    <FooterMainWrapper>
      <FooterTopContainer>
        <FooterMenuContainer>
          <FooterMenuBox>
            <FooterMenuItem>Про нас</FooterMenuItem>
            <FooterMenuItem>Новини</FooterMenuItem>
            <FooterMenuItem>Атлети</FooterMenuItem>
            <FooterMenuItem>Послуги Атлетам</FooterMenuItem>
            <FooterMenuItem>Контакти</FooterMenuItem>
          </FooterMenuBox>
          <FooterMenuBox>
            <FooterMenuItem>Види спорту та правила</FooterMenuItem>
            <FooterMenuItem>Безпека атлетів</FooterMenuItem>
            <FooterMenuItem>Здоров'я атлетів</FooterMenuItem>
            <FooterMenuItem>Зробити пожертву</FooterMenuItem>
            <FooterMenuItem>Безпека атлетів</FooterMenuItem>
          </FooterMenuBox>
          <FooterMenuBox>
            <FooterMenuItem>Спонсори</FooterMenuItem>
            <FooterMenuItem>Партнери</FooterMenuItem>
            <FooterMenuItem>Тренувальні центри</FooterMenuItem>
            <FooterMenuItem>Для преси</FooterMenuItem>
            <FooterMenuItem>Заявити подію</FooterMenuItem>
          </FooterMenuBox>
        </FooterMenuContainer>

        <MailAndFollowContainer>
          <SendMailBox>
            <h3>Отримуйте новини та анонси</h3>
            <div>
              <input type="email" placeholder='someone@example.com'/>
              <button type="submit">Підписатися</button>
            </div>
          </SendMailBox>

          <FollowBox>
            <FollowIconsBar>
              <UseAnimations animation={facebook}
                             size={44} strokeColor="#ffffff" color="#ffffff"
                             wrapperStyle={{cursor: "pointer"}} />
              <UseAnimations animation={instagram}
                             size={44} strokeColor="#ffffff" color="#ffffff"
                             wrapperStyle={{cursor: "pointer"}} />
              <UseAnimations animation={linkedin}
                             size={44} strokeColor="#ffffff" color="#ffffff"
                             wrapperStyle={{cursor: "pointer"}} />
              <UseAnimations animation={twitter}
                             size={44} strokeColor="#ffffff" color="#ffffff"
                             wrapperStyle={{cursor: "pointer"}} />
              <UseAnimations animation={youtube}
                             size={44} strokeColor="#ffffff" color="#ffffff"
                             wrapperStyle={{cursor: "pointer"}} />
            </FollowIconsBar>
          </FollowBox>
        </MailAndFollowContainer>
      </FooterTopContainer>
      <CopyrightContainer />
      <Love />
    </FooterMainWrapper>
  );
};

export default Footer;
