import React from 'react';
import {Copyright} from './Copyright.style';
import {FaRegCopyright} from 'react-icons/fa';

const CopyrightContainer = () => {
  return (
    <Copyright>
      <FaRegCopyright />
      <span>Українська Ліга Тріатлону. Всі права зарезервовано. {' '}
        {new Date().getFullYear()}
        {'.'}</span>
    </Copyright>
  );
};

export default CopyrightContainer;
