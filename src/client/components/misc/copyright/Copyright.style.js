import styled from 'styled-components';

export const Copyright = styled.div`
  display: flex;
  width: 100%;
  height: 50px;
  color: #ffffff;
  align-items: center;
  justify-content: center;
  user-select: none;

  & span {
    margin-left: 7px;
  }
`;
