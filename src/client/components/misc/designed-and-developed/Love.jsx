import React from 'react';
import UseAnimations from 'react-useanimations';
import activity from 'react-useanimations/lib/activity';

const Love = () => {
  return (
    <div style={{
      position: "absolute",
      display: "flex",
      bottom: 0,
      right: 40,
      alignItems: "center",
      justifyContent: "center",
      color: "#ffffff"
    }}>
      S<UseAnimations animation={activity}
                      size={30} strokeColor = "#ffffff" color="#ffffff"
                      wrapperStyle={{cursor: "none"}} />M
    </div>
  );
}

export default Love;
