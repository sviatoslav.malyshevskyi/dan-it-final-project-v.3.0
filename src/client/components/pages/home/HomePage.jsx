import React from 'react';
import {HomePageMainWrap} from './HomePage.style';

const HomePage = () => {
  return (
    <HomePageMainWrap>
      <p>TOP CAROUSEL SECTION</p>
    </HomePageMainWrap>
  );
};

export default HomePage;
