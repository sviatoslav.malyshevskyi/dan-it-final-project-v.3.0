import styled from 'styled-components';
import {FiUser} from 'react-icons/fi';

export const HeaderMainContainer = styled.header`
  width: 100%;
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  user-select: none;
`;

export const HeaderRow = styled.div`
  display: flex;
  height: 110px;
  align-items: center;
  justify-content: space-evenly;
  color: #ffffff;
  background-color: #002464;
`;

export const HeaderLogoContainer = styled.div`
  width: 20%;
`;

export const WebsiteTitleContainer = styled.div`
  margin-bottom: 11px;
  font: bold 40px Montserrat, Lato, sans-serif;

  & h1 {
    text-shadow: 3px 6px rgba(0, 0, 0, 0.75);
    user-select: none;
  }
`;

export const HeaderIconsContainer = styled.div`
  display: flex;
  width: 14%;
  align-items: center;
  justify-content: space-evenly;
  color: #ffffff;
`;

export const HeaderBottom = styled(HeaderRow)`
  display: flex;
  margin: 0 12% 0 12%;
  justify-content: flex-end;
  height: 70px;
  color: #002464;
  background-color: #ffffff;
`;

export const HeaderBottomIcons = styled(HeaderIconsContainer)`
  margin-top: 15px;

  & .fa-user-circle {
    color: #002464;
    cursor: pointer;
  }
`;

export const UserWidget = styled(FiUser)`
  position: absolute;
  right: 12%;
  width: 60px;
  height: 60px;
  padding-left: 5%;
  font-weight: 200;
  color: #002464;
  cursor: pointer;
`;
