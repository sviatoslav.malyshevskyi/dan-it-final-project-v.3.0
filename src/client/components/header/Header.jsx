import React from 'react';
import {
  HeaderBottom, HeaderBottomIcons, HeaderMainContainer, HeaderIconsContainer,
  HeaderLogoContainer, HeaderRow, UserWidget, WebsiteTitleContainer,
} from './Header.style';
import UseAnimations from 'react-useanimations';
import menu2 from 'react-useanimations/lib/menu2';
import {IoLanguageSharp} from 'react-icons/io5';

const Header = () => {
  return (
    <HeaderMainContainer>
      <HeaderRow>
        <HeaderLogoContainer />
        <WebsiteTitleContainer>
          <h1>Українська Ліга Тріатлону</h1>
        </WebsiteTitleContainer>
        <HeaderIconsContainer>
          <IoLanguageSharp size={35} strokeColor='#ffffff'
                           style={{cursor: "pointer"}} />
          <UseAnimations animation={menu2}
                         size={50} strokeColor='#ffffff' color='#ffffff'
                         wrapperStyle={{cursor: "pointer"}} />
        </HeaderIconsContainer>
      </HeaderRow>

      <HeaderBottom>
        <HeaderBottomIcons>
          <UserWidget />
        </HeaderBottomIcons>
      </HeaderBottom>
    </HeaderMainContainer>
  );
};

export default Header;
